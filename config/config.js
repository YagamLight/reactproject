export default {
    plugins: [
      ['umi-plugin-react', {
        antd: true,
        dva:true,
      }],
    ],
    proxy: {
       "/api/v1": {
        "target": "http://localhost:4040",
        "changeOrigin": true,
        "pathRewrite": { "^/api/v1" : "/api/v1" }
         }
      },
    routes: [{
      path: '/',
      component: '../layout',
      routes: [
        {
          path: '/',
          component: './Helloworld/helloworld',
        },
        {
          path: '/hello',
          component: './Helloworld/helloworld'
        },
        {
          path: '/first',
          component: './FirstCompent/firstcompent'
        },
        {
          path: '/student',
          component: './Student/student'
        },
        {
          path: '/firstmodel',
          component: './firstmodel/firstmodel'
        },
        {
          path: '/websocket',
          component: './Websocket/websocket'
        },
      ]
    }],
  }