import{Table} from 'antd'
import {Component} from 'react'
export default class studenttable extends Component{
    render(){
        const {data}=this.props.datasource==null?[]:this.props.datasource;
        const columns=[
            {
                title:"ID",
                dataIndex:'id',
            },
            {
                title:"姓名",
                dataIndex:'name',
            },
            {
                title:"年龄",
                dataIndex:'age',
            },
            {
                title:"性别",
                dataIndex:'sex',
            }
        ]
        return(
            <Table columns={columns} dataSource={data}></Table>
        );
    }
}