import React ,{ Component }from 'react'
import { Input,Form, Card, Modal, message, Button, Row, Col } from 'antd'
import Studenttable from '../Student/studentTable'
import { connect } from 'dva'
import { multipleValidOptions } from '_jest-validate@24.8.0@jest-validate/build/condition';
@connect(state => ({
  student: state.student,
}))
export default class Student extends Component{
  onClick=()=>{
    const { dispatch } = this.props;
    // this.setState({
    //   formValues: {},
    //   editData: {},
    //   defaultCurrent: 1,
    //   pageSize: 10,
    // });
    dispatch({
      type: 'student/fetch',
    });
  }
  render(){
    return (
      <div>
      <Button onClick={this.onClick} >查询</Button>
      <Studenttable
       datasource ={this.props.student==null?null:this.props.student}
      ></Studenttable>
      </div>
    );
  }
}