import request from '@/utils/request';
import { stringify } from 'qs';
//获取数据带分页
export async function queryAll(params) {
  return request(`/api/v1/hello/student/queryAll?${stringify(params)}`);
}