import { queryAll, changeRole, queryOrg, Add, remove, edit } from '../services/student';

export default {
    namespace: 'student',
    state: {
        data: [],
        loading: false,
    },

    effects: {
        *fetch(payload, { call, put }) {
            const response = yield call(queryAll, payload.payload);
            yield put({
                type: 'save',
                payload: response,
            });
        },
    },

    reducers: {
        save(state, action) {
            return {
                ...state,
                data: action.payload,
            };
        },

    },
};
