
import { Mentions, Form, Button} from 'antd'
import React,{Component} from 'react'
export default class send extends Component {
  handleReset = e => {
    e.preventDefault();
    this.props.form.resetFields();
  };

  checkMention = (rule, value, callback) => {
    const mentions = getMentions(value);
    if (mentions.length < 2) {
      callback(new Error('More than one must be selected!'));
    } else {
      callback();
    }
  };
  render() {
      debugger;
    const {
      getFieldDecorator,
      sendMessage
    } = this.props;
    return (
      <Form layout="horizontal">
        <Form.Item label="Top coders" labelCol={{ span: 6 }} wrapperCol={{ span: 16 }}>
          {getFieldDecorator('mention', {
            rules: [{ validator: this.checkMention }],
          })(
            <Mentions rows="3">
              <Option value="afc163">afc163</Option>
              <Option value="zombieJ">zombieJ</Option>
              <Option value="yesmeck">yesmeck</Option>
            </Mentions>,
          )}
        </Form.Item>
        <Form.Item wrapperCol={{ span: 14, offset: 6 }}>
          <Button type="primary" onClick={this.props.children.sendMessage("")}>
            Submit
          </Button>
          &nbsp;&nbsp;&nbsp;
          <Button onClick={this.handleReset}>Reset</Button>
        </Form.Item>
      </Form>
    );
  }
}