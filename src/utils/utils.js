import moment from 'moment';
import React from 'react';
import { parse, stringify } from 'qs';
export function isAntdPro() {
    return window.location.hostname === 'preview.pro.ant.design';
  }